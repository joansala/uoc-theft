Práctica final - GTA de zombies
===============================

¿Qué es?
--------



Vídeo de demostración
---------------------

![Demo](Resources/demo.webm)

¿Qué se ha implementado?
------------------------

*

Detalles de la implementación
-----------------------------



La última versión
-----------------

Puede encontrar información sobre la última versión de este software y su
desarrollo actual en https://gitlab.com/joansala/uoc-theft

Referencias
-----------

Todos los modelos 3D y sonidos del juego se han publicado con licéncias que
permiten la reutilización y distribución. Son propiedad intelectual de sus
respectivos authores. Algunos de los recursos se han creado o modificado
exclusivamente para su uso en el juego por el autor, Joan Sala Soler.

GunShot
https://freesound.org/people/okieactor/sounds/415912/

Bullet Hole
https://picsart.com/i/sticker-art-hole-268803725027211

Books
https://assetstore.unity.com/packages/3d/props/interior/books-3356

Quick female scream
https://freesound.org/people/AmeAngelofSin/sounds/168725/

Pain: ouch
https://freesound.org/people/AmeAngelofSin/sounds/234039/

Short scared scream
https://freesound.org/people/jorickhoofd/sounds/180344/

Blood Hitting Window
https://freesound.org/people/Rock%20Savage/sounds/81042/

Zombie Voice
https://freesound.org/people/PatrickLieberkind/sounds/213835/
